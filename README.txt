This software is unlicenced and free to use in any commerical/non-commercial software and for educational/research purposes.

Requirements:
requires boost library (specifically the boost/shared_ptr.h header)

Usage:
The RandomTree class is templated with the following template<class DataPoint, typename RandomFunctor, typename DataSplitCalculator, typename RandomTreeNode>:

Requirements:
DataPoint
    must implement 
	    bool operator==(const DataPoint& other) 
		operator!=(const DataPoint& other)
		
RandomFunctor:
    must implement 
	    double operator()(DataPoint& data_point)
		
DataSplitCalculator:
    must implement
	    data_split<DataPoint, RandomFunctor> getBestDataSplit(std::vector<DataPoint>& data, std::vector<RandomFunctor>& functors, double current_depth);
		where data_split<DataPoint, RandomFunctor> is defined in the RandomTree header file
		
RandomTreeNode:
	void mark_as_leaf()
	void set_data(std::vector<DataPoint>& data)
	void set_functor(RandomFunctor& functor);
	void set_threshold(double threshold);
	void create_left_child();
	void create_right_child();
	boost::shared_ptr<RandomTreeNode> get_left_child();
	boost::shared_ptr<RandomTreeNode> get_right_child();

