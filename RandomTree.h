/*
 * RandomTree.h
 *
 *  Created on: 18 Dec 2012
 *      Author: aly
 */

#ifndef RANDOMTREE_H_
#define RANDOMTREE_H_
#include <vector>
#include <boost/shared_ptr.hpp>

template<class DataPoint, typename RandomFunctor>
struct data_split {
		std::vector<DataPoint> left_data;
		std::vector<DataPoint> right_data;
		RandomFunctor functor;
		double threshold;
		data_split():threshold(0){};
		data_split(std::vector<DataPoint>& left_data, std::vector<DataPoint>& right_data, RandomFunctor& functor, double threshold)
				:left_data(left_data), right_data(right_data),functor(functor), threshold(threshold)
		{};
};

template<class DataPoint, typename RandomFunctor, typename DataSplitCalculator, typename RandomTreeNode>
class RandomTree {
	
private:
	boost::shared_ptr<RandomTreeNode> root;
	DataSplitCalculator datasplit_calculator;
	
	bool data_has_same_values(std::vector<DataPoint>& data){
		DataPoint first = *(data.begin());

		for(typename std::vector<DataPoint>::iterator it = data.begin() + 1; it != data.end(); it++){
			if(*it != first){
				return false;
			}
		}
		
		return true;
	}
	
	void build(boost::shared_ptr<RandomTreeNode>& current_node, std::vector<DataPoint>& data, 
			std::vector<RandomFunctor>& functors, int max_depth, int min_number_per_node, int current_depth)
	{
		
		if(current_depth == max_depth|| data.size() <= min_number_per_node || data_has_same_values(data))
		{
			current_node->mark_as_leaf();
			current_node->set_data(data);
			return;
		}
		
		data_split<DataPoint, RandomFunctor> data_split = datasplit_calculator.getBestDataSplit(data, functors, current_depth);
		current_node->set_functor(data_split.functor);
		current_node->set_threshold(data_split.threshold);
			
		if(data_split.left_data.size() == 0 || data_split.right_data.size() == 0){
			current_node->mark_as_leaf();
			current_node->set_data(data);
			return;
		}
					
		current_node->create_left_child();
		current_node->create_right_child();
		build(current_node->get_left_child(), data_split.left_data, functors, max_depth, min_number_per_node, current_depth + 1);
		build(current_node->get_right_child(), data_split.right_data, functors, max_depth, min_number_per_node, current_depth + 1);
	}
	
public:
	RandomTree(){};
	
	RandomTree(std::vector<DataPoint>& data, std::vector<RandomFunctor>& functors,
			int max_depth, int min_number_per_node,
			DataSplitCalculator& datasplit_calculator,
			boost::shared_ptr<RandomTreeNode> root = boost::shared_ptr<RandomTreeNode>(new RandomTreeNode())
			):
				root(root), datasplit_calculator(datasplit_calculator){
		
		build(root, data, functors, max_depth, min_number_per_node, 0);
	}
	
	
	boost::shared_ptr<RandomTreeNode> get_root(){
		return root;
	}
	
	~RandomTree(){};
};

#endif /* RANDOMTREE_H_ */
